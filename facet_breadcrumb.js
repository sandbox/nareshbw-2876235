(function($) {
  $(document).ready(function() {
	  
    var output = '';
    var color = ['#E55C00', '#005CB9', '#9C97FF', '#1D5F11', '#DE2533', '#E55C00', '#005CB9', '#9C97FF', '#1D5F11', '#DE2533'];
    var counter = 0
    $('.span-one .region-sidebar-first .facetapi-active').each(function(key) {
      label = $(this).children('span.element-invisible').text();
      label = label.trim();
      if (label) {
        url = $(this).attr('href');
        label = label.replace('Remove ', '');
        label = label.replace(' filter', '');
        if(colorArr[label]){
          tagColor = colorArr[label];
        }else{
          tagColor = color[counter];
          counter++;
        }
        output += '<li style="background-color:'+tagColor+';"><a href="' + url + '">' + label + ' X</a></li>';
      }
    });
    $('.view-calendar .date-heading').after('<ul class="span-unset">' + output + '</ul>');
})(jQuery);